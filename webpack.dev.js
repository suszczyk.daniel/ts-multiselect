const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	entry: {
		main: './src/index.ts',
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'ts-multiselect.js',
		library: 'ts-multiselect',
		libraryTarget: 'umd',
	},
	resolve: {
		extensions: ['.ts', '.js'],
	},
	devtool: 'source-map',
	devServer: {
		open: true,
	},
	module: {
		rules: [
			{
				test: /\.ts?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.(sass|scss|css)$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{
						loader: 'css-loader',
						options: {
							modules: false,
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
						},
					},
				],
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'style.css',
		}),
		new HtmlWebpackPlugin({
			template: 'src/index.html',
			inject: false,
			hash: false,
			filename: 'index.html',
		}),
	],
};