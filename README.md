# Ts-Multiselect

This is a very basic, lightweight dropdown with multi (and single) selection written in typescript (no jQuery).


Examples: [StackBlitz](https://stackblitz.com/edit/ts-multiselect?devtoolsheight=33&file=index.ts)

## Installation

- Add package with:
```
npm install ts-multiselect
```
- Add styles to your global styles file:
```
@import "~ts-multiselect/dist/style.css";
```

## Usage

- Create an element and give him unique id:
```
<div id="my-multiselect"></div>
```
- Instantiate Multiselect class and pass config:
```
import {Multiselect} from 'ts-multiselect';

const myMultiselect = new Multiselect({
  id: 'my-multiselect',
  options: [
    {
      value: 1,
      label: 'Awesome,
    },
  ],
});
```
- Simply get selected items by:
```
console.log(myMultiselect.selected);
```

## Config

### Inputs:

Field | Type | Description | Required | Default
--- | --- | --- | --- | ---
id | string | Unique id for element where multiselect will be rendered | Yes | 
options | IMultiselectOption[] | An array of objects (see IMultiselectOption section)| Yes | []
multiple | boolean | Allow single or multiple selection | No | true
singularNominativeLabel | string | Label for one selected item (see Language section) | No | 
pluralNominativeLabel | string | Label for 2-4 selected items (see Language section) | No | 
pluralGenitiveLabel | string | Label for 5+ selected items (see Language section) | No | 
placeholder | string | Displayed in selection container when no item is selected | No |
headerLabel | string | Displayed above selection container (aka label) | No | 
selected | any[] | Default selection. Values should be the same as `value` field in options | No | []

### Outputs:

Field | Parameters | Description | Required
--- | --- | --- | ---
onDropdownOpen | () => any | Emits event when dropdown opens | No
onDropdownClose | (selectedItems: any[]) => any | Emits event when dropdown closes | No
onSelectionChange | (selectedItems: any[]) => any | Emits event when selection is changed | No

### Methods and properties:

Field | Description
--- | ---
init() | It creates elements and appends it to container. Ypu dont have to call it after class instantiate.
destroy() | Removes all elements from DOM and all listeners. If you want to create the same multiselect just call init() method .
reset() | Clear selection and show placeholder (if provided).
hide() | Clear DOM but leave all references and current selection. You can simply render multiselect after hiding and it will looks like never hidden.
render() | Show multiselect after hiding.
selected | Get selected items.
rendered | Determine if multiselect is rendered.

### IMultiselectOption interface

Simply pass two fields to make an option:

Field | Type | Description | Required
--- | --- | --- | ---
value | any (but preferred number or string due to initial select comparison) | Value which will be passed in selected array. | Yes
label | string | Label to show in dropdown or in selection-container | Yes


## Language

When item is selected (single item in single mode or one item in a multiple mode)
it will display an item label in selection container.

When 2 items (only in a multiple mode) are selected you will see `2 items` in selection container by default.
You can add plural forms in options (in Polish it would be for example):
```
singularNominativeLabel: 'porcja', // 1 porcja
pluralNominativeLabel: 'porcje', // 2 porcje
pluralGenitiveLabel: 'porcji', // 5 porcji
```
If it does not affect your language, you can leave these fields empty.



